package com.example.alen.animationpredavanje.androidfragmentexample.scrolabletabs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.alen.animationpredavanje.R;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentOne;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentThree;
import com.example.alen.animationpredavanje.androidfragmentexample.viewpageradapter.CustomAdapter;

import java.util.ArrayList;
import java.util.List;

import devlight.io.library.ntb.NavigationTabBar;

public class ScrollableTabsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolable_tabs);

        final ViewPager viewPager = findViewById(R.id.viewPager);
        final NavigationTabBar navigationTabBar = findViewById(R.id.ntb);
        final String[] colors = getResources().getStringArray(R.array.default_preview);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.facebook),
                        Color.parseColor(colors[0])
                ).title("Heart")
                        .badgeTitle("NTB")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.instagram),
                        Color.parseColor(colors[0])
                ).title("Cup")
                        .badgeTitle("with")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                                getResources().getDrawable(R.drawable.linkedin),
                                Color.parseColor(colors[0])
                        ).title("Diploma")
                                .badgeTitle("state")
                                .build()
                );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.facebook),
                        Color.parseColor(colors[0])
                ).title("Flag")
                        .badgeTitle("icon")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.instagram),
                        Color.parseColor(colors[0])
                ).title("Medal")
                        .badgeTitle("777")
                        .build()
        );

        navigationTabBar.setModels(models);
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new FragmentOne());
        fragmentList.add(new FragmentThree());
        fragmentList.add(new FragmentOne());
        fragmentList.add(new FragmentThree());
        CustomAdapter adapter = new CustomAdapter(getSupportFragmentManager(),fragmentList );
        viewPager.setAdapter(adapter);
        navigationTabBar.setViewPager(viewPager, 1);

        navigationTabBar.setTitleMode(NavigationTabBar.TitleMode.ACTIVE);
        navigationTabBar.setBadgeGravity(NavigationTabBar.BadgeGravity.BOTTOM);
        navigationTabBar.setBadgePosition(NavigationTabBar.BadgePosition.CENTER);
        navigationTabBar.setTypeface("fonts/custom_font.ttf");
        navigationTabBar.setIsBadged(true);
        navigationTabBar.setIsTitled(true);
        navigationTabBar.setIsTinted(true);
        navigationTabBar.setIsBadgeUseTypeface(true);
        navigationTabBar.setBadgeBgColor(Color.RED);
        navigationTabBar.setBadgeTitleColor(Color.WHITE);
        navigationTabBar.setIsSwiped(true);
        navigationTabBar.setBgColor(Color.BLACK);
        navigationTabBar.setBadgeSize(10);
        navigationTabBar.setTitleSize(10);
        navigationTabBar.setIconSizeFraction((float) 0.5);

    }
}
