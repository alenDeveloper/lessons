package com.example.alen.animationpredavanje.retrofitexample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MoviesProviderClient {

    @GET("/?apikey=bb16fd94")
    Call<MovieModel> getMovie(@Query("t") String title);

    @GET("/?apikey=bb16fd94")
    Call<SearchedMovieModel> getSearchedMovie(@Query("s") String title, @Query("page") Integer page);
}
