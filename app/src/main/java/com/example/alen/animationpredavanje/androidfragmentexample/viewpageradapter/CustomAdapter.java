package com.example.alen.animationpredavanje.androidfragmentexample.viewpageradapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.ListView;

import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentFive;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentFour;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentOne;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentThree;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentTwo;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapter extends FragmentPagerAdapter {
    List<Fragment> fragmentList = new ArrayList<>();

    public CustomAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
