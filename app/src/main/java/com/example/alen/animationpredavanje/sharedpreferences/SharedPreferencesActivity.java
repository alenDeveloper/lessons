package com.example.alen.animationpredavanje.sharedpreferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.alen.animationpredavanje.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SharedPreferencesActivity extends AppCompatActivity {

    final String TAG = getClass().getSimpleName();

    private RecyclerView mainList;
    private MainListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);

        setRecyclerAdapter();
    }


    public void storeAndRetrieveString() {

        SharedPreferences sharedPreferences = this.getSharedPreferences(SharedPreferencesConstants.SHARED_PREFERENCES, MODE_PRIVATE);

        sharedPreferences.edit().putString(SharedPreferencesConstants.SHARED_USERNAME, "Alen").apply();

        String name = sharedPreferences.getString(SharedPreferencesConstants.SHARED_USERNAME, "OK");

        Log.i(TAG, name);
    }

    public void storeAndRetrieveList() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(SharedPreferencesConstants.SHARED_PREFERENCES, MODE_PRIVATE);

        ArrayList<String> list = new ArrayList<>();

        list.add("Alen");
        list.add("Semir");
        list.add("Nedim");

        try {
            sharedPreferences.edit().putString(SharedPreferencesConstants.SHARED_LIST, ObjectSerializer.serialize(list)).apply();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<String> listTwo = new ArrayList<>();
        try {
            listTwo = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString(SharedPreferencesConstants.SHARED_LIST, ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!listTwo.isEmpty()) {
            Log.i(TAG, listTwo.toString());
        }

    }

    public void setRecyclerAdapter() {

        mainList = (RecyclerView) findViewById(R.id.main_recycler_list);

        List<NewsObject> newsObjectList = new ArrayList<>();

        NewsObject object1 = new NewsObject();
        object1.setTitle("Title1");
        object1.setDescription("Description 1 Description 1 Description 1 Description 1 Description 1 Description 1 Description 1 Description 1");
        object1.setImageUrl("http://www.clker.com/cliparts/M/b/g/K/G/F/simple-newspaper-md.png");

        NewsObject object2 = new NewsObject();
        object2.setTitle("Title2");
        object2.setDescription("Description 1 Description 1 Description 1 Description 1 Description 1 Description 1 Description 1 Description 1");
        object2.setImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7K8rrBnWfbqO6vucMCpUjRE96QhfVKjj1V9HvBSp0lbvP3Mtc");

        NewsObject object3 = new NewsObject();
        object3.setTitle("Title3");
        object3.setDescription("Description 1 Description 1 Description 1 Description 1 Description 1 Description 1 Description 1 Description 1");
        object3.setImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6sXUIucH9jJVQK8uNHePIxuql3jDgJcx8zOGlp_IxR2Rih7aI");

        NewsObject object4 = new NewsObject();
        object4.setTitle("Title4");
        object4.setDescription("Description 1 Description 1 Description 1 Description 1 Description 1 Description 1 Description 1 Description 1");
        object4.setImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzMXpE50vPNoO6wg7z2dDLEfK8yA-4ZiOgyjkQJYEH2GBfxhJM");

        newsObjectList.add(object1);
        newsObjectList.add(object2);
        newsObjectList.add(object3);
        newsObjectList.add(object4);

        newsObjectList.add(object1);
        newsObjectList.add(object2);
        newsObjectList.add(object3);
        newsObjectList.add(object4);

        adapter = new MainListAdapter(newsObjectList);
        mainList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mainList.setAdapter(adapter);
    }
}
