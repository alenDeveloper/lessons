package com.example.alen.animationpredavanje.androidfragmentexample.tabs;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.alen.animationpredavanje.R;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentOne;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentThree;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentTwo;

import java.util.ArrayList;
import java.util.List;

public class TabIconActivity extends AppCompatActivity {

    private List<Fragment> fragmentList = new ArrayList<>();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabIconAdapter iconTabsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_icon);

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabs);

        prepareDataResource();

        iconTabsAdapter = new TabIconAdapter(getSupportFragmentManager(),fragmentList);

        viewPager.setAdapter(iconTabsAdapter);
        tabLayout.setupWithViewPager(viewPager);

        setTabsIcons();
    }

    private void setTabsIcons() {
        int i = 0;
        while (i < fragmentList.size()) {
            if (i % 2 == 0) {
                tabLayout.getTabAt(i).setIcon(R.drawable.facebook).setText("FACEBOOK");
            } else {
                tabLayout.getTabAt(i).setIcon(R.drawable.instagram).setText("INSTAGRAM");
            }

            i++;
        }
    }

    private void prepareDataResource() {

        fragmentList.add(new FragmentOne());
        fragmentList.add(new FragmentTwo());
        fragmentList.add(new FragmentThree());
        fragmentList.add(new FragmentOne());
        fragmentList.add(new FragmentTwo());
        fragmentList.add(new FragmentThree());
        fragmentList.add(new FragmentThree());
        fragmentList.add(new FragmentTwo());
        fragmentList.add(new FragmentThree());
        fragmentList.add(new FragmentThree());
    }
}
