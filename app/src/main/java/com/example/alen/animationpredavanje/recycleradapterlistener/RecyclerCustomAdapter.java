package com.example.alen.animationpredavanje.recycleradapterlistener;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alen.animationpredavanje.R;
import com.example.alen.animationpredavanje.sharedpreferences.MainListAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerCustomAdapter extends RecyclerView.Adapter<RecyclerCustomAdapter.ViewHolder> {

    List<CarItem> carItems = new ArrayList<>();
    Context context;
    OnContactClick onContactClick;

    public void setClickListener(OnContactClick clickListener) {
        this.onContactClick = clickListener;
    }

    public RecyclerCustomAdapter(Context context, List<CarItem> list) {
        this.carItems = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_list_item, parent, false);
        return new RecyclerCustomAdapter.ViewHolder(view, onContactClick);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CarItem carItem = carItems.get(position);

        holder.title.setText(carItem.title);
        holder.desc.setText(carItem.description);
        Picasso.with(context).load(carItem.imageUrl).into(holder.image);
        holder.item = carItem;
    }

    @Override
    public int getItemCount() {
        return carItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, desc;
        ImageView image;
        TextView contactText;

        CarItem item;

        public ViewHolder(View itemView, final OnContactClick click) {
            super(itemView);

            title = itemView.findViewById(R.id.list_car_title);
            desc = itemView.findViewById(R.id.car_description);
            image = itemView.findViewById(R.id.list_car_image);
            contactText = itemView.findViewById(R.id.contact_text);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    click.onContactClick(item);
                }
            };

            contactText.setOnClickListener(listener);
        }
    }

    public interface OnContactClick {
        void onContactClick(CarItem item);
    }
}
