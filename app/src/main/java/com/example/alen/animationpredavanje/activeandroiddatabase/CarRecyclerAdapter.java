package com.example.alen.animationpredavanje.activeandroiddatabase;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alen.animationpredavanje.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CarRecyclerAdapter extends RecyclerView.Adapter <CarRecyclerAdapter.ViewHolder>{

    List<CarDetails> carDetails;
    Context context;
    OnItemLongClick listener;

    public CarRecyclerAdapter(Context context, List<CarDetails> carDetails, OnItemLongClick listener) {
        this.carDetails = carDetails;
        this.context = context;
        this.listener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_details_custom_row, parent, false);
        return new CarRecyclerAdapter.ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CarDetails carDetail = carDetails.get(position);

        holder.title.setText(carDetail.name);
        holder.price.setText(String.valueOf(carDetail.price) + " $");
        if (carDetail.imageUrl != null && !carDetail.imageUrl.isEmpty()) {
            Picasso.with(context).load(carDetail.imageUrl).error(R.drawable.search).into(holder.image);
        }
        holder.carDetails = carDetail;

    }

    @Override
    public int getItemCount() {
        return carDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, price;
        ImageView image;
        CarDetails carDetails;

        public ViewHolder(View itemView, final OnItemLongClick listener) {
            super(itemView);

            title = itemView.findViewById(R.id.car_list_name);
            price = itemView.findViewById(R.id.car_price);
            image = itemView.findViewById(R.id.car_list_image);

            title.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.onDetailsCarItemLongClick(carDetails);
                    return false;
                }
            });
            image.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.onDetailsImageItemClick(carDetails.imageUrl);
                    return false;
                }
            });
        }
    }

    public interface OnItemLongClick {
        void onDetailsCarItemLongClick(CarDetails car);
        void onDetailsImageItemClick(String imageUrl);
    }
}
