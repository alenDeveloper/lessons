package com.example.alen.animationpredavanje;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    TextView timeText;
    Button btnStart;
    ListView mainList;
    ArrayAdapter adapter;
    ArrayList<String> data;
    boolean isRunning = false;
    Handler handler;
    Runnable runnable;
    int seconds = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        mainList = (ListView) findViewById(R.id.main_list);
        timeText = (TextView) findViewById(R.id.time_text);
        btnStart = (Button) findViewById(R.id.btn_text);

        handler = new Handler();

        data = new ArrayList<>();
        data.add("Times:");

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);

        mainList.setAdapter(adapter);

    }

    public void startStopwatch(View view) {
        if (!isRunning) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    seconds++;
                    setTime(seconds);
                    handler.postDelayed(this, 1000);
                }
            };

            handler.post(runnable);
            btnStart.setText("STOP");
            isRunning = true;
        } else {
            handler.removeCallbacks(runnable);
            btnStart.setText("START");
            isRunning = false;
        }
    }

    public void addItem(View view) {
        adapter.add(timeText.getText().toString());
        adapter.notifyDataSetChanged();
    }

    public void setTime(int second) {
        String minute = String.valueOf((int) second / 60);
        String seconds = second < 10 ? "0" + String.valueOf((int) second & 60) : String.valueOf((int) second & 60);

        timeText.setText(minute + ":" + seconds);
    }
}
