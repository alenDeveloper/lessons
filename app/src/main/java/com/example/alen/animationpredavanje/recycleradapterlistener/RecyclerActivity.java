package com.example.alen.animationpredavanje.recycleradapterlistener;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.alen.animationpredavanje.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    private RelativeLayout mainHolder;
    private RecyclerView mainListRecycler;
    private RecyclerCustomAdapter adapter;
    private TextView emailText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        mainHolder = findViewById(R.id.main_holder);
        mainListRecycler = findViewById(R.id.recycler_list);
        emailText = findViewById(R.id.mail_text);

        setAdapter();
    }

    public void setAdapter() {
        List<CarItem> carItems = new ArrayList<>();

        carItems.add(new CarItem("Golf V (8 000 KM)", "http://cdn1.autoexpress.co.uk/sites/autoexpressuk/files/styles/article_main_image/public/2016/11/db2016au00913_large.jpg?itok=tqct249F",
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis",
                "mailgolf@email.com"));
        carItems.add(new CarItem("Audi a3 (7 000 KM)", "http://cdn1.autoexpress.co.uk/sites/autoexpressuk/files/styles/article_main_image/public/2016/06/a3-fd-238_1.jpg?itok=PEoidzHS",
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis",
                "mailaudi@email.com"));
        carItems.add(new CarItem("Mercedes A klasa (10 000 KM)", "https://adriaticmedianethr.files.wordpress.com/2015/07/gallery_view.jpg?quality=100&strip=all&w=620&h=400&crop=1",
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, " +
                        "nascetur ridiculus mus. Donec quam felis Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis",
                "mailmercedes@email.com"));

        RecyclerCustomAdapter.OnContactClick clickListener = new RecyclerCustomAdapter.OnContactClick() {
            @Override
            public void onContactClick(CarItem item) {
                emailText.setText(item.email);
                mainHolder.setVisibility(View.VISIBLE);
            }
        };

        adapter = new RecyclerCustomAdapter(getApplicationContext(), carItems);
        adapter.setClickListener(clickListener);
        mainListRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(mainListRecycler);
        mainListRecycler.setAdapter(adapter);
    }

    public void closeMailHolder(View view) {
        mainHolder.setVisibility(View.GONE);
    }
}
