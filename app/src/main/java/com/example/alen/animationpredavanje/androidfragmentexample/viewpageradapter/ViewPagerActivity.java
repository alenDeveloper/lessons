package com.example.alen.animationpredavanje.androidfragmentexample.viewpageradapter;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.alen.animationpredavanje.R;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentFive;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentFour;
import com.example.alen.animationpredavanje.androidfragmentexample.fragments.FragmentThree;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        ViewPager viewPager = findViewById(R.id.viewPager);

        List<Fragment> list = new ArrayList<>();
        list.add(new FragmentFive());
        list.add(new FragmentThree());
        list.add(new FragmentThree());
        list.add(new FragmentFive());
        list.add(new FragmentThree());
        list.add(new FragmentThree());
        list.add(new FragmentFive());
        list.add(new FragmentFour());
        list.add(new FragmentThree());

        CustomAdapter adapter = new CustomAdapter(getSupportFragmentManager(), list);
        viewPager.setAdapter(adapter);
    }
}
