package com.example.alen.animationpredavanje.sharedpreferences;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alen.animationpredavanje.R;
import com.squareup.picasso.Picasso;

public class NewsActivity extends AppCompatActivity {

    TextView title;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        title = (TextView) findViewById(R.id.news_activity_title);
        image = (ImageView) findViewById(R.id.news_activity_image);

        Intent i = getIntent();
        String newsTitle = i.getStringExtra("title");
        String imageUrl = i.getStringExtra("image");


        title.setText(newsTitle);
        Picasso.with(this).load(imageUrl).into(image);
    }
}
